#system
import sys
from collections import defaultdict
import copy
#package
import pygame
import apng
import OpenGL as lse
import OpenGL.GL as lily 
from pygame.locals import *
import pyglet as glet
from pyglet.gl import *
from direct.showbase.ShowBase import ShowBase
from direct.showbase.Loader import Loader
import panda3d
from panda3d.core import NodePath
from panda3d.core import Quat
#__app__
import lib
from lib import file_open
from lib import classproperty
from lib import Setting
import __fight_for_liberty.fight_for_liberty as ffl

class UsePg:

    def __init__(self):
        JSONOBJ = Setting.getJson()
        #self.__SCRFLG = ((pygame.OPENGL|pygame.DOUBLEBUF),)3Dが必要な時
        self.__game_fps:int = JSONOBJ["spd"]
        self.__game_name:str = JSONOBJ["title"]
        pygame.init()
        self.__wnd = pygame.display.set_mode((JSONOBJ["scr"]["x"],JSONOBJ["scr"]["y"]),JSONOBJ["flg"]|pygame.FULLSCREEN if JSONOBJ["is-full-screen"] else 0) 
        pygame.display.set_caption(self.__game_name)
        pygame.time.Clock()
        self.__wnd.fill((0,0,255))
        
    def Quit(self):
        self.__game.fin_Game()
        pygame.quit()
        try:
            sys.exit()
        except BaseException:
            pass

    def postEvent(self,aevent):
        if(aevent.type == pygame.QUIT):
            self.Quit()
            return pygame.QUIT
        if(aevent.type == pygame.KEYDOWN):
            if(aevent.key == pygame.K_ESCAPE):
                self.Quit()
                return pygame.QUIT
            else:
                self.__game.upd_Dw_Key(aevent.key)
                return
        if(aevent.type == pygame.KEYUP):
            self.__game.upd_Up_Key(aevent.key)
        if(aevent.type == pygame.MOUSEMOTION):
            (x, y) = aevent.pos
            self.__game.mouse_Pos(x,y)
        else:
            return

    def isCanLoop(self):
        is_looping=True
        for event in pygame.event.get():
            if self.postEvent(event) is pygame.QUIT:
                is_looping=False
        return is_looping

    def run(self):
        self.__game = ffl.Vak(self)
        while(self.isCanLoop()):
            self.__game.game_Loop()
            pygame.display.update()
        return self

    @property
    def wnd(self):
        return self.__wnd 

class BaseApp(ShowBase):
 
    def __init__(self):
        super().__init__(self)
        self.app=ffl.Vak(self)
        self.app.Init()
        

    def keyUpdate(self,code,cls,fnc,args=[]):
        self.accept(code,eval("cls."+fnc),args)

    def Update(self):
        self.app.Update3()
        self.run()

    @property
    def control(self):
        return self.getControl()
    
    @property
    def shift(self):
        return self.getShift()

    @property
    def wnd(self):
        return self

class Use3d():

    def __init__(self):
        pass

    def run(self):
        self.app = BaseApp()
        try:
            self.app.Update()
        except BaseException:
            pass

class UseApp():

    def __init__(self):
        pass

    def run(self):
        JSONOBJ = Setting.getJson()
        try:
            config=Config(sample_buffers=1,samples=4,depth_size=24,double_buffer=True)
            self.window=glet.window.Window(JSONOBJ["scr"]["x"],JSONOBJ["scr"]["y"],JSONOBJ["title"],resizable=True,config=config)
        except glet.window.NoSuchConfigException:
            self.window=glet.window.Window(JSONOBJ["scr"]["x"],JSONOBJ["scr"]["y"],JSONOBJ["title"],resizable=True)
        return

class App():

    def __init__(self):
        jobj=Setting.getJson()
        if jobj["use-class"] == "3d":
            self.app=Use3d()
        elif jobj["use-class"] == "2d":
            self.app=UsePg()
        elif jobj["use-class"] == "app":
            self.app=UseApp()

    def run(self):
        self.app.run()

    def getApp(self):
        return self.app

#Mainclass
class Main(metaclass=lib.PropertyMeta):
    #静的変数
    myapp=App()

    #main関数
    @classmethod
    def main(cls):
        cls.myapp.run()

    @classproperty
    def app(cls):
        return cls.myapp.getApp()

if __name__ == "__main__":
    Main.main()
