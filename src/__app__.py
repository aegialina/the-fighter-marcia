import importlib
import os
import stat
import json
import __main__
import lib
from lib import file_open
import subprocess
from subprocess import PIPE
import platform

if platform.system()=="Windows":
    try:
        main = importlib.import_module("pip._internal.main")
    except Exception:
        print("https://bootstrap.pypa.io/get-pip.py からダウンロードして実行してください。")
    pass

else:
    try:
        main = importlib.import_module("pip._internal.main")
    except Exception:
        gst="curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py;sudo python3 get-pip.py --force-reinstall"
        subprocess.call(gst,stdout=subprocess.PIPE,shell=True)

def arg():
    with file_open(f"{__file__}.json") as f:
        return json.load(f)

def _import(name, module, ver=None):
    try:
        globals()[name] = importlib.import_module(module)
    except ImportError:
        main(["install","--upgrade","pip"])
        try:
            if ver is None:
                main(['install', module])
            else:
                main(['install', '{}=={}'.format(module, ver)])
            globals()[name] = importlib.import_module(module)
        except Exception:
            print("can't import: {}".format(module))

def chmod(name):
    os.chmod(name,stat.S_IXOTH)

config=arg()
    
for need in config["needs"]:
    try:
        _import(need["as-name"],need["module"], need["ver"])
    except Exception as e:
        print(need["ver"])
        print(e)

for script in config["os.chmod"]:
    chmod(script)

import StarColoredNightSky

StarColoredNightSky.Main.main()
