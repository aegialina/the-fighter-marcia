import StarColoredNightSky as scns
from StarColoredNightSky import Quat
from StarColoredNightSky import Setting
from lib_main import *
import json
import pyson
from lib import file_open

class Vak:

    #コンストラクタ(クラス初期化)
    def __init__(self,base):#thisの部分はselfやthisが一般的
        self.exp=Exp(base.wnd)
        #アンダーバー*2でプライベート 
        #self.__FULLNESS = (22222,)定数宣言(大文字タプル)アクセスはthis.__FULLNESS[0]
        #Render対象作成
        #exp/self(self).__render_name = sk.Render("Data\\name.png",posx,posy)
        #self.__scene = Scene()
        with file_open(Setting.getJson()["ffl.arg"]) as f:
            self.jobj=json.load(f)
        with file_open(self.jobj["res"]) as f:
            self.res=json.load(f)
        self.back=Clist()[self.jobj["back-color"]]
        self.base=base
        pass#何もしないCの様な無名関数は不要<-インタプリタが読み飛ばしてくれる。

    def update_A(self):
        self.cube1.quat=self.cube1.quat+panda3d.core.Quat( 0, 0.2, 0, 0 )
    
    def forward(self):
        self.cube1.y=self.cube1.y - 1

    def backward(self):
        self.cube1.y=self.cube1.y + 1

    def go_right(self):
        self.cube1.x=self.cube1.x + 1

    def go_left(self):
        self.cube1.x=self.cube1.x - 1

    def turn_up(self):
        self.cube1.p=self.cube1.p - 10

    def turn_down(self):
        self.cube1.p=self.cube1.p + 10

    def turn_right(self):
        self.cube1.h=self.cube1.h - 10

    def turn_left(self):
        self.cube1.h=self.cube1.h + 10

    #初期化処理(毎回)
    def Init(self):
        self.key_loop=KeyLoop(base)
        if(self.jobj["use-pyson"]):
            self.pyobj=pyson.load(self.jobj["pyson"])
            print(self.pyobj["test"])
        GameObject(self.base,"models/environment",(0,0,0))
        GameObject(self.base,"models/misc/rgbCube",(-6,20,0)).scale=0.2
        self.cube1=GameObject(self.base,"models/misc/rgbCube",(6,20,0))
        self.cube1.scale=1.8
        self.cube1.quat=Quat( 0, 1, 1, 1 )
        for k in self.pyobj["key"]:
            base.keyUpdate(k["types"],self,k["def"])
            self.key_loop[k["types"]]=eval("self."+k["def"])
        for res in self.jobj["res-key"]:
            with file_open(self.res[res].format(res_dir=self.res["res-dir"])) as f:
                print(f.read())
        return

    #キーの入力時の更新処理
    def upd_Dw_Key(self,akey):
        #if key == K_Up:
        return

    #キーの出力時の更新処理
    def upd_Up_Key(self,akey):
        return

    #マウス座標取得
    def mouse_Pos(self,ax,ay):
        return

    def Update3(self):
        for k,_ in self.key_loop.items():
            self.key_loop[k]()

    #更新処理
    def Update(self):
        print(self.back.float)
        return

    #描画処理
    def Draw(self):
        #exp/self.__render_name.Draw()
        return

    #音楽再生処理 pygame.mixer系
    def Music(self):
        return

    #ゲームループ
    def game_Loop(self):
        if(False):#発動しない
            self.Init()
        self.Update()
        #スクリーンに色出力
        self.exp.draw_Wnd(self.back.rgb)
        self.Draw()
        self.Music()
        pass

    #終了処理
    def fin_Game(self):
        return
