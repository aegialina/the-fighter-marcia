import pkgutil
import json

class classproperty(property):
    pass

class PropertyMeta(type):

    def __new__(cls, name, bases, namespace):
        props = [(k, v) for k, v in namespace.items() if type(v) == classproperty]
        for k, v in props:
            setattr(cls, k, v)
            del namespace[k]
        return type.__new__(cls, name, bases, namespace)

def file_open(name):
    try:
        return open(name,"r",encoding="utf-8")
    except:
        pass
    try:
        f=name.split('.pyz')
        ret=pkgutil.get_data(f[0],f[1])
        print(ret)
        return ret
    except:
        return None

class textwnd(object):
    __app = []
    def __init__(self):
        self.__app.append(wx.App())

    def cslshow(self,title,string,flg):
        return wx.MessageBox(string,title,flg)
  
class Setting:
    argobj=None
    @classmethod
    def getJson(cls):
        if cls.argobj is None:
            with file_open("arg.json") as f:
                cls.argobj=json.load(f)
        return cls.argobj

class Loader():
    If=lambda expr,true,false:true() if expr else false()
    Emit=lambda value,max:[value for i in range(max)]
    Each=lambda value,done:[done(l)for l in value]
    Add=lambda v1,v2:v1+v2
    Sub=lambda v1,v2:v1-v2
    Len=lambda vlist:range(len(vlist))
    Define=lambda done,**p:eval(done)

    @staticmethod
    def Import(path):
        try:
            with file_open(path) as fln:
                ret=eval(fln.read())
                if ret==type(dict):
                    return ret
                else:
                    return {}
        except:
            return {}

    Def={
        "if":If,"emit":Emit,"each":Each,"add":Add,"sub":Sub,"len":Len,
        "sum":sum,"def":Define,"import":Import
        }

    @staticmethod
    def setSrc(src,namespace):
        ds=src.split(".")
        try:
            return f"{ds[0]}.{namespace}.set{ds[1].capitalize()}"
        except:
            return ""

    @staticmethod
    def getSrc(src,namespace):
        ds=src.split(".")
        try:
            return f"{ds[0]}.{namespace}.get{ds[1].capitalize()}()"
        except:
            return "0"

    def AppendSrc(self,src,namespace):

        "self.cube1.y append 1"

    def purce(self,code):
        ns=code.split(" ")
        if ns[0]=="def":
            try:
                self.Def[ns[0]](ns[1],eval(ns[2:]))
            except:
                self.Def[ns[0]](ns[1])
        elif len(ns)==2:
            try:
                self.Def[ns[0]](eval(ns[1]))
            except:
                pass
        elif len(ns)==3:
            pass
        pass
        #if expr,true,false
        #emit v1,size
        #def expr,p,p2...
        #a.b append c =>a.${on}.setB(a.${on}.getB()+c)
        #a.b reduce c =>a.${on}.setB(a.${on}.getB()-c)
        #a.b set c add d =>a.${on}.setB(c+d)
        #a.b set a.b add c =>a.${on}.setB(a.${on}.getB()+c)
        #a.b set c sub d =>a.${on}.setB(c-d)

    def __init__(self,code):
        self.code=code.splitlines()
        for cd in self.Len(self.code):
            purce(self.code[cd])
