from collections import defaultdict
import pygame
import panda3d
import lib
from lib import classproperty
from lib import Setting

class Exp(metaclass=lib.PropertyMeta):
    exp=None
    def __init__(self,wnd):
        self.__wnd=wnd
        if self.exp is None:
            self.exp=self

    def Write(self,font,string,x,y,sd,color,bc=None):
        if bc is None:
            self.__wnd.blit(font.render(string,sd,color.Get(),bc.Get()),(x,y))
        else:
            self.__wnd.blit(font.render(string,sd,color.Get()),(x,y))

    def Draw(self,serf,x,y,cutting=None):
        if cutting is None:
            self.__wnd.blit(surf,(x,y))
        else:
            self.__wnd.blit(surf,(x,y),cutting)

    def WindowUpdate(self,acolor):
        self.__wnd.fill(acolor)

    def MakeSurf(self,path):
        return pygame.image.load(path).convert_alpha()

    def draw_Wnd(self,acolor):
        self.exp.WindowUpdate(acolor)

class Render:

    def __init__(self,apath,ax,ay):
        self.path = apath
        self.x = ax
        self.y = ay
        self.surf = Exp.exp.MakeSurf(self.path)
    
    def get_Size_Get(self):
        return (-self.surf.get_width(),-self.surf.get_height())

    def Draw(self):
        Exp.exp.Draw(self.surf,self.x,self.y)
    
    def draw_Middle(self):
        Exp.exp.Draw(self.surf,self.x-self.surf.get_width()/2,self.y-self.surf.get_height()/2)

    def cut_Draw(self,astart_x,astart_y,acut_x,acut_y):
        Exp.exp.Draw(self.surf,self.x,self.y,(astart_x,astart_y,acut_x,acut_y))
    
    def Rev(self,abx,aby):
        self.surf = pygame.transform.flip(self.surf,abx,aby)

    def Scale(self,asx,asy):
        self.surf = pygame.transform.scale(self.surf,(asx,asy))

    def Rot(self,aangle):
        self.surf = pygame.transform.rotate(self.surf.convert(),aangle)

class Hits_Flg:

    #widthをposzに入れる
    def Tri(pos1,pos2,h2):
        if(pos1.posx + pos1.posz <= pos2.posx 
           or pos1.posx >= pos2.posx + pos2.posz):
            return False
        elif(pos2.posy + h2 >= pos1.posy 
             or pos1.posy <= pos2.posy + h2):
            return False
        else:
            return True

    def Yen(pos0,pos1):
        posx = pos1.x - pos0.x
        posy = pos1.y - pos0.y
        rad = pos1.z + pos0.z
        if(pow(posx,2) + pow(posy,2) 
           <= pow(rad,2)):
            return True
        else:
            return False
        pass

    def is_Inside(left,middle,right):
        return middle > left and middle < right

class Pos():
    x:int
    y:int
    z:int
    def __init__(self,posx,posy,btwn):
        self.x = posx
        self.y = posy
        self.z = btwn

class Pos3D:

    def __init__(self,pos_x,pos_y,pos_z,size_x = 0,size_y = 0):
        self.pos = (pos_x,pos_y,pos_z)
        self.size = (size_x,size_y,0)
        return 

    def location_In_View(self,dj_x, dj_y):
        #""" 3D座標から2D上の座標算出 """
        x2 = int((self.pos.x + dj_x * self.pos.y) / (self.pos.z/10 + 1)) + 200 - int(self.size.x * 0.5 / (self.pos.z/10 + 1))
        y2 = int((self.pos.y + dj_y * self.pos.z) / (self.pos.z/10 + 1)) + 150 - int(self.size.y * 0.5 / (self.pos.z/10 + 1)) 
        return (x2, y2)

    def size_In_View(self):
        #""" 3D座標から2D上のサイズ算出 """
        s_x2 = int(self.size.x / (self.pos.z/10 + 1))
        s_y2 = int(self.size.y / (self.pos.z/10 + 1))
        return (s_x2, s_y2)

class ColorInt():
    __alpha:int
    __red:int
    __green:int
    __blue:int
    def __init__(self,r,g,b,a=256):
        self.__alpha = a
        self.__red = r
        self.__green = g
        self.__blue = b

    def GetRgb(self):
        return(self.__red,self.__green,self.__blue)

    
    def GetRgba(self):
        return(self.__red,self.__green,self.__blue,self.__alpha)

    def ToFloat(self):
        a=float(self.__alpha)/256
        r=float(self.__red)/256
        g=float(self.__green)/256
        b=float(self.__blue)/256
        return(r,g,b,a)

    def ToInt(self):
        return (self.__red,self.__green,self.__blue,self.__alpha)

class ColorFloat():
    __alpha:int
    __red:int
    __green:int
    __blue:int
    def __init__(self,r,g,b,a=256):
        self.__alpha = a
        self.__red = r
        self.__green = g
        self.__blue = b

    def GetRgb(self):
        return(self.__red,self.__green,self.__blue)
    
    def GetRgba(self):
        return(self.__red,self.__green,self.__blue,self.__alpha)

    def ToInt(self):
        a=int(self.__alpha*256)
        r=int(self.__red*256)
        g=int(self.__green*256)
        b=int(self.__blue*256)
        return(r,g,b,a)

    def ToFloat(self):
        return (self.__red,self.__green,self.__blue,self.__alpha)

#色定義
class Color:

    def __init__(self,_type,r,g,b,a=0):
        if _type == int:
            self.color=ColorInt(r,g,b,a)
        elif _type == float:
            self.color=ColorFloat(r,g,b,a)
        self._type=_type

    @property
    def rgb(self):
        return self.color.GetRgb()

    @property
    def rgba(self):
        return self.color.GetRgba()

    def __Make(self,_type,lst):
        return Color(_type,lst[0],lst[1],lst[2],lst[3])

    def __str__(self):
        ret="Color{"
        lst=self.color.GetRgba()
        ret+=f'red:{lst[0]},green:{lst[1]},blue:{lst[2]},alpha:{lst[3]}'+'}'
        return ret

    @property
    def float(self):
        if self._type==float:
            return copy.copy(self.color)
        elif self._type==int:
            return self.__Make(float,self.color.ToFloat())

    @property
    def int(self):
        if self._type==int:
            return copy.copy(self.color)
        elif self._type==float:
            return self.__Make(int,self.color.ToInt())

class CIntlist:
    #色 = Color(color_type,紅, 緑, 青)
    White = Color(int,255, 255, 255)
    Black = Color(int,0, 0, 0)
    Green = Color(int,0, 255, 0)
    Blue = Color(int,0, 0, 255)
    Red = Color(int,255, 0, 0) 
    Yellow = Color(int,255, 255, 0)
    Purple = Color(int,255, 0, 255)
    Skyblue = Color(int,0, 255, 255)
    Orange = Color(int,255, 180, 105)
    Blown = Color(int,165, 42, 42)
    Pink = Color(int,255, 0, 165)
    Silver = Color(int,192, 192, 192)
    Lime = Color(int,50, 205, 50)
    Virid = Color(int,0, 137, 107)
    Coblue = Color(int,0, 71, 171)
    Dblown = Color(int,81, 31, 9)
    Greeny = Color(int,17, 69, 20)

class CFloatlist:
    #色 = Color(color_type,紅, 緑, 青)
    White = Color(float,1, 1, 1)
    Black = Color(float,0, 0, 0)
    Green = Color(float,0, 1, 0)
    Blue = Color(float,0, 0, 1)
    Red = Color(float,1, 0, 0) 
    Yellow = Color(float,1, 1, 0)
    Purple = Color(float,1, 0, 1)
    Skyblue = Color(float,0, 1, 1)
    Orange = Color(float,1, 180, 105)
    Blown = Color(float,165, 42, 42)
    Pink = Color(float,1, 0, 165)
    Silver = Color(float,192, 192, 192)
    Lime = Color(float,50, 205, 50)
    Virid = Color(float,0, 137, 107)
    Coblue = Color(float,0, 71, 171)
    Dblown = Color(float,81, 31, 9)
    Greeny = Color(float,17, 69, 20)

#色リスト(自分で好きな色拡張してね)
class Clist(defaultdict):

    __colors=CIntlist()if Setting.getJson()["use-class"]=="2d"else CFloatlist()if Setting.getJson()["use-class"]=="3d" else None

    def __init__(self):
        c=self.__colors
        self.default_factory = c.White
        self.update(White=c.White,Black=c.Black,Green=c.Green,Blue=c.Blue
            ,Red=c.Red,Yellow=c.Yellow,Purple=c.Purple,Skyblue=c.Skyblue
            ,Orange=c.Orange,Blown=c.Blown,Pink=c.Pink,Silver=c.Silver
            ,Lime=c.Lime,Virid=c.Virid,Coblue=c.Coblue,Dblown=c.Dblown
            ,Greeny=c.Greeny,Fenhongse=c.Pink,Hongse=c.Red
            )

class Cvmoveflg():

    def __init__(self,is_up,is_dw,is_lt,is_rt):
        self.up = is_up
        self.down = is_dw
        self.left = is_lt
        self.right = is_rt

class CvFont:

    def __init__(self,name,size,color=CIntlist.Black):
        self.font = pygame.font.Font(name,size)
        self.color = color

    def upd_color(self,color):
        self.color = color

    def show(self,string,sd,x,y):
        Exp.exp.Write(self.font,string,x,y,sd,self.color)
        
    def bc_show(self,string,sd,bc,x,y):
        Exp.exp.Write(self.font,string,x,y,sd,self.color,bc)

    def Font(self,color=CIntlist.White):
        self.color = color

class GameResrc():
    def __init__(self, base,path):
        self.obj=path

class Music(GameResrc):
    def __init__(self,base,path):
        self.music=base.loader.loadMusic()
        self.path=path

class GameObject(GameResrc):

    def __init__(self,base,path,pos):
        #tex= base.loader.loadTexture(path)
        self.obj = base.loader.loadModel(path)
        self.obj.reparentTo(base.render)
        self.obj.setScale(1, 1, 1)
        try:
            self.obj.setPos(pos[0],pos[1],pos[2])
        except Exception:
            self.obj.setPos(0,0,0)
    
    @property
    def scale(self):
        return self.obj.getScale()

    @scale.setter
    def scale(self,_scale):
        self.obj.setScale(_scale)

    @property
    def quat(self):
        return self.obj.getQuat()

    @quat.setter
    def quat(self,_quat):
        try:
            self.obj.setQuat(_quat)
        except Exception:
            return

    @property
    def x(self):
        return self.obj.getX()

    @x.setter
    def x(self,pos):
        self.obj.setX(pos)

    @property
    def y(self):
        return self.obj.getY()

    @y.setter
    def y(self,pos):
        self.obj.setY(pos)

    @property
    def h(self):
        return self.obj.getH()

    @h.setter
    def h(self,pos):
        self.obj.setH(pos)

    @property
    def p(self):
        return self.obj.getP()

    @h.setter
    def p(self,pos):
        self.obj.setP(pos)

    @property
    def r(self):
        return self.obj.getR()

    @h.setter
    def r(self,pos):
        self.obj.setR(pos)

    @property
    def turn(self):
        return self.obj.getHpr()

    @turn.setter
    def turn(self,hpr):
        try:
            self.obj.setHpr(hpr[0],hpr[1],hpr[2])
        except Exception:
            pass

class Texture(GameResrc):
    def __init__(self, base, path):
        self.obj = base.loader.loadTexture(path)

class KeyLoop(dict):

    def __init__(self,base):
        self.my=defaultdict(lambda:lambda:None)
        self.is_down = base.mouseWatcherNode.is_button_down
        self.get_key=panda3d.core.KeyboardButton.ascii_key

    def __getitem__(self,getter):
        if self.is_down(self.get_key(getter)):
            return self.my[getter]
        else:
            return lambda:None

    def __setitem__(self, key, fn):
        self.my[key]=fn


class Player(GameObject):

    def __init__(self, base, path, pos,direct):
        self.obj = direct.actor.Actor(path,direct)
        try:
            self.obj.setScale(pos[0],pos[1],pos[2])
        except Exception:
            self.obj.setScale(0,0,0)
        self.obj.reparentTo(base.render)
