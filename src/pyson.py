import sys
import argparse
import re
import zipapp
import io
import lib
from lib import file_open
import types
import copy
cp=copy.copy
dcp=copy.deepcopy

class File(dict):
    fn=type(lambda:None)
    location=None
    @classmethod
    def miller(cls,value):
        if os.name == 'nt':
            value.replace("fast/",value[len("fast/")+1]+":/")
        elif os.name == 'posix':
            return value.replace("fast",'')
        else:
            return value.replace("fast",'')

    @classmethod
    def get_location(cls):
        if cls.location:
            return cls.location
        elif os.path.isfile("root.pyson"):
            cls.location=cls.Load("root.pyson")
            return cls.location
        else:
            return {}

    @classmethod
    def getRef(cls,object,tag):
        try:
            return object[int(tag)]
        except:
            try:
                return object[tag]
            except:
                return None

    @classmethod
    def GetMod(cls,self,value,lst):
        string=value.split(" ")
        if "$ref" == string[0]:
            try:
                my=string[1].split(".")
                if my[0]=="this":
                    obj=dcp(self)
                    for tag in my[1:]:
                        try:
                            obj=cls.getRef(obj,tag)
                        except Exception as e:
                            return None
                    return obj
                else:
                    obj=cls.Load(f"{cls.miller(my[0].replace('root','..'))}.pyson")
                    for tag in my[1:]:
                        try:
                            obj=cls.getRef(obj,tag)
                        except Exception as e:
                            return None
                    return obj
            except Exception as e:
                return self
        elif "$public" == string[0]:  
            string.sprit()
            return value
        elif "$private" == string[0]:
            string.sprit()
            return value
        elif "$this" == string[0]:
            return self
        else:
            return value

    @classmethod
    def GetValue(cls,self,value,lst):
        for i in value:
            my=type(i)
            if my==int:
                continue
            elif my==float:
                continue
            elif my==cls.fn:
                continue
            elif my==str:
                try:
                    value[i]=cls.GetMod(self,value[i],lst)
                except:
                    pass
            elif my==list or my==tuple or my==dict:
                lst.append(i)
                cls.GetValue(self,value[i],lst)

    @classmethod
    def Load(cls,path):
        data = None
        with file_open(path) as fln:
            var = []
            for f in fln.read():
                if('__import__' in f.split('(')) or ('def' in f.split(' ')):
                    return None
                else:
                    var.append(f)
            data = cls.__Ld(var)
        try:
            ret = eval(data)
            cls.GetValue(ret,ret,[])
        except:
            ret = None
            print("pysonファイルが不適切なため実行できません")
        return ret

    @classmethod
    def __Ld(cls,var):
        ret = ""
        for val in var:
            ret += val
        return ret

    @classmethod
    def Using(cls,value,namespace=[]):
        obj = value
        for var in namespace:
            try:
                obj = obj[var]
            except expression as ex:
                return None
        return obj

    @classmethod
    def ZipConfig(cls,lst:list):
        tmp = io.BytesIO()
        zipapp.create_archive("setting.piz",tmp)
        with open ('setting.piz' , 'wb' ) as f:
            f.write(tmp.getvalue())

load = File.Load
using = File.Using
rdpiz = File.ZipConfig